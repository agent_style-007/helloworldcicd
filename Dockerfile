FROM androidsdk/android-30

WORKDIR project/
RUN chmod -R 0777 /opt/android-sdk-linux
CMD ["./gradlew assembleDebug"]